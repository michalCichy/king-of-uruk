#include "stdafx.h"
#include "Config.h"
#include "GLFW.h"

#include "Renderer.h"
#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "Texture.h"
#include "World.h"
#include "ShaderManager.h"
#include "TextureManager.h"

void handleInput(Mouse & mouse, Keyboard & keyboard) {
}

int main()
{
	GLFW::init();
	Renderer::init();
	ShaderManager::init();
	TextureManager::init();

	Mouse mouse;
	Keyboard keyboard;

	World world;

	while (!glfwWindowShouldClose(GLFW::getWindow())) {
		GLFW::readInput();
		mouse.readInput();
		keyboard.readInput();

		handleInput(mouse, keyboard);
		world.handleInput(mouse, keyboard);
		world.handleEvents();

		Renderer::clear();

		world.draw();

		glfwSwapBuffers(GLFW::getWindow());

		ErrorHandler::handleErrors();

		//Sleep(Config::getFrameDelay());
	}

	GLFW::exit();
	return 0;
}
