#pragma once

#include "stdafx.h"
#include "Texture.h"
#include "Config.h"

class TextureManager {
private:
	static Texture groundTexture;
	static Texture buildingTexture;
	static Texture wallTexture;
	static Texture sunTexture;
	static Texture swordTexture;
	static Texture palmTexture;

public:
	static void init() {
		loadTextures();
	}

	static Texture* getGroundTexture() {
		return &groundTexture;
	}

	static Texture* getBuildingTexture() {
		return &buildingTexture;
	}

	static Texture* getWallTexture() {
		return &wallTexture;
	}

	static Texture* getSunTexture() {
		return &sunTexture;
	}

	static Texture* getSwordTexture() {
		return &swordTexture;
	}

	static Texture* getPalmTexture() {
		return &palmTexture;
	}

private:
	static void loadTextures() {
		groundTexture.init(Config::getTexturesPath() + "ground2.jpg");
		buildingTexture.init(Config::getTexturesPath() + "sandwall.jpg");
		wallTexture.init(Config::getTexturesPath() + "concrete.jpg");
		sunTexture.init(Config::getTexturesPath() + "sun.png");
		swordTexture.init(Config::getTexturesPath() + "sword.jpg");
		palmTexture.init(Config::getTexturesPath() + "palm.png");
	}
};

Texture TextureManager::groundTexture;
Texture TextureManager::buildingTexture;
Texture TextureManager::wallTexture;
Texture TextureManager::sunTexture;
Texture TextureManager::swordTexture;
Texture TextureManager::palmTexture;