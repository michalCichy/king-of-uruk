#version 330 core
layout(location = 0) in vec3 inPosition
layout(location = 1) in vec2 uv;

uniform float cameraYaw;
uniform float cameraPitch;

out uv;

void main() {

	//gl_Position = rotate(mat4(1), -cameraPitch, vec3(1,0,0)) * rotate(mat4(1), -cameraYaw, vec3(0,1,0)) * vec4(inPosition, 1.0);
	gl_Position = vec4(inPosition, 1.0);
	uv = inUv;
};