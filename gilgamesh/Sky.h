#pragma once

#include "stdafx.h"
#include "Mesh.h"
#include "ShaderManager.h"
#include "TextureManager.h"

class Sky {
private:
	Mesh sun;
	float sunPosition = 0;
	float sunSpeed = 0.001;
	vec4 skyColor = Config::getSkyColor();
	
	Lighting lighting;

public:
	Sky() {
		vector <Vertex> vertices{
			Vertex(vec3(0.5f,  0.5f, 0.0f),   vec2(1.0f, 1.0f), vec3(0,0,1)),
			Vertex(vec3(0.5f, -0.5f, 0.0f),   vec2(1.0f, 0.0f), vec3(0,0,1)),
			Vertex(vec3(-0.5f, -0.5f, 0.0f),   vec2(0.0f, 0.0f), vec3(0,0,1)),
			Vertex(vec3(-0.5f,  0.5f, 0.0f),   vec2(0.0f, 1.0f), vec3(0,0,1))
		};
		vector <unsigned int> indices = {
			0, 1, 3,
			1, 2, 3 
		};

		mat4 sunInitialTransformation = glm::translate(mat4(1), vec3(0, 10, 0)) * glm::scale(mat4(1), vec3(10, 10, 10)) * glm::rotate(mat4(1), Constants::HALF_PI, Constants::X_AXIS);
		sun.init(vertices, indices, ShaderManager::getMainShader(), TextureManager::getSunTexture(), sunInitialTransformation, 1, true);
		
		lighting = Lighting(0.3, 0.3, 0.3, vec3(0, -1, 0));
	}

	void handleDayPassing() {
		sunPosition += sunSpeed;

		if (sunPosition > Constants::DOUBLE_PI) {
			sunPosition -= Constants::DOUBLE_PI;
		}
		else if (sunPosition < -Constants::DOUBLE_PI) {
			sunPosition += Constants::DOUBLE_PI;
		}

		sun.setPitch(sunPosition);

		float skyBrightness = (cos(sunPosition) + 1) / 2;
		
		skyColor = Config::getSkyColor() * skyBrightness;
		lighting.ambient = 0.8 * skyBrightness + 0.2;
	}

	void draw(Camera& camera) {
		glDisable(GL_DEPTH_TEST);
		Renderer::drawColor(skyColor);
		
		Lighting lighting = Lighting(1, 0, 0, vec3(0, 0, 0)); //HAHA O�WIETLENIE S�O�CA
		sun.draw(camera, lighting);
		glEnable(GL_DEPTH_TEST);
	}

	Lighting& getLighting() {
		return lighting;
	}
};