#pragma once

#include "stdafx.h"
#include "Camera.h"

class Player {
private:
	Camera camera;
	vec3 position;
	float height = 2;

public:
	Player() {
		position = Config::getPlayerInitialPosition();
		vec3 cameraTranslation = position;
		cameraTranslation.y = height;
		camera.move(cameraTranslation);
	}

	Camera& getCamera() {
		return camera;
	}

	void handleInput(Mouse& mouse, Keyboard& keyboard) {
		camera.rotate(-mouse.getMovement().y * Config::getMouseSensitivity(), mouse.getMovement().x * Config::getMouseSensitivity());

		vec3 forward = glm::normalize(glm::cross(Constants::WORLD_UP, camera.getRight()));
		vec3 left = camera.getLeft();
		vec3 right = camera.getRight();
		vec3 back = -forward;

		float boost = 1;

		if (keyboard.isShiftPressed()) {
			boost = Config::getPlayerBoost();
		}

		if (keyboard.isUpPressed()) {
			position += forward * Config::getPlayerSpeed() * boost;
		}
		if (keyboard.isDownPressed()) {
			position += back * Config::getPlayerSpeed() * boost;
		}
		if (keyboard.isLeftPressed()) {
			position += left * Config::getPlayerSpeed() * boost;
		}
		if (keyboard.isRightPressed()) {
			position += right * Config::getPlayerSpeed() * boost;
		}
		
		vec3 cameraPosition = position;
		cameraPosition.y = height;
		camera.setPosition(cameraPosition);
	}
private:
};