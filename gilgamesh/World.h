#pragma once

#include "stdafx.h"
#include "Camera.h"
#include "GameObject.h"
#include "Shader.h"
#include "Mesh.h"
#include "Importer.h"
#include "Cube.h"
#include "Ground.h"
#include "Primitives.h"
#include "ShaderManager.h"
#include "TextureManager.h"
#include "Sky.h"
#include "Player.h"

class World {
private:
	Sky sky;
	vector <GameObject> objects;
	vector <Mesh> palms;
	Mesh ground;
	Camera globalCamera;
	Player player;
	Mesh sword;
	float swordPitch = 0;
	float change = 0.01;

public:
	World() {
		loadGameObjects();

		globalCamera = Camera(vec3(0, 10, 10));
	}

	void handleInput(Mouse& mouse, Keyboard& keyboard) {
		if (Config::getCameraMode() == CAMERA_GLOBAL) {
			globalCamera.handleInput(mouse, keyboard);
		}
		else {
			player.handleInput(mouse, keyboard);
		}

		if (swordPitch > 0.2 || swordPitch < -0.2) {
			change = -change;
		}
		swordPitch += change;
		sword.rotatePitch(change);
	}

	void handleEvents() {
		sky.handleDayPassing();
	}

	void draw() {
		sky.draw(getActiveCamera());
		ground.draw(getActiveCamera(), sky.getLighting());

		for (auto& object : objects) {
			object.draw(getActiveCamera(), sky.getLighting());
		}
		sword.draw(getActiveCamera(), sky.getLighting());

		for (auto& palm : palms) {
			palm.draw(getActiveCamera(), sky.getLighting());
		}
	}

private:
	void loadGameObjects() {
		mat4 groundInitialTransformation = glm::scale(mat4(1), Config::getGroundDimensions());
		ground = Mesh(Primitives::Plane::getVertices(), Primitives::Plane::getIndices(), ShaderManager::getMainShader(), TextureManager::getGroundTexture(), groundInitialTransformation, 100);


		int range = 100;
		int offset = 10;

		float height = 5;

		for (int z = -range / 2; z <= range / 2; z += offset) {
			for (int x = -range / 2; x <= range / 2; x += offset) {
				if (x != 0 && z != 0) {
					mat4 initialTransformation = glm::translate(mat4(1), vec3(x, 0.5 * height, z)) * glm::scale(mat4(1), vec3(4, height, 4));

					GameObject building = GameObject(Mesh(
						Primitives::Cube::getVertices(),
						Primitives::Cube::getIndices(),
						ShaderManager::getMainShader(),
						TextureManager::getWallTexture(),
						initialTransformation
					));

					objects.push_back(building);
				}
			}
		}

		/*int wallHeight = 20;
		objects.push_back(GameObject(Mesh(
			Primitives::Cube::getVertices(),
			Primitives::Cube::getIndices(),
			ShaderManager::getMainShader(),
			TextureManager::getWallTexture(),
			glm::translate(mat4(1), vec3(0, wallHeight / 2, -Config::getGroundDimensions().z / 2)) * glm::scale(mat4(1), vec3(Config::getGroundDimensions().x, wallHeight, 1))
		)));
		objects.push_back(GameObject(Mesh(
			Primitives::Cube::getVertices(),
			Primitives::Cube::getIndices(),
			ShaderManager::getMainShader(),
			TextureManager::getWallTexture(),
			glm::translate(mat4(1), vec3(Config::getGroundDimensions().x / 2, wallHeight / 2, 0)) * glm::scale(mat4(1), vec3(1, wallHeight, Config::getGroundDimensions().z))
		)));
		objects.push_back(GameObject(Mesh(
			Primitives::Cube::getVertices(),
			Primitives::Cube::getIndices(),
			ShaderManager::getMainShader(),
			TextureManager::getWallTexture(),
			glm::translate(mat4(1), vec3(0, wallHeight / 2, Config::getGroundDimensions().z / 2)) * glm::scale(mat4(1), vec3(Config::getGroundDimensions().x, wallHeight, 1))
		)));
		objects.push_back(GameObject(Mesh(
			Primitives::Cube::getVertices(),
			Primitives::Cube::getIndices(),
			ShaderManager::getMainShader(),
			TextureManager::getWallTexture(),
			glm::translate(mat4(1), vec3(-Config::getGroundDimensions().x / 2, wallHeight / 2, 0)) * glm::scale(mat4(1), vec3(1, wallHeight, Config::getGroundDimensions().z))
		)));*/

		mat4 swordInitialTransformation =  glm::translate(mat4(1), vec3(0.5, 0.6, -1)) * glm::scale(mat4(1), vec3(0.05, -0.05, 0.05)) * glm::rotate(mat4(1), -Constants::HALF_PI, Constants::Y_AXIS);
		
		sword = Mesh(Config::getModelsPath() + "sword1/sword1.obj", ShaderManager::getMainShader(), TextureManager::getSwordTexture(), swordInitialTransformation, 1, false, true);


		float palmsRange = 150;
		float palmsStep = 30;
		for (float x = -palmsRange / 2; x <= palmsRange / 2; x += palmsStep) {
			float z = glm::sqrt((palmsRange / 2) * (palmsRange / 2) - x * x);
			mat4 initialTransformation = glm::translate(mat4(1), vec3(x, 10, z)) * glm::scale(mat4(1), vec3(10,20,10));

			palms.push_back(Mesh(
				Primitives::Quad::getVertices(),
				Primitives::Quad::getIndices(),
				ShaderManager::getSpriteShader(),
				TextureManager::getPalmTexture(),
				initialTransformation
			));

			initialTransformation = glm::translate(mat4(1), vec3(x, 10, -z)) * glm::scale(mat4(1), vec3(10, 20, 10));

			palms.push_back(Mesh(
				Primitives::Quad::getVertices(),
				Primitives::Quad::getIndices(),
				ShaderManager::getSpriteShader(),
				TextureManager::getPalmTexture(),
				initialTransformation
			));

		}
	}

	Camera& getActiveCamera() {
		if (Config::getCameraMode() == CAMERA_GLOBAL) {
			return globalCamera;
		}
		else {
			return player.getCamera();
		}
	}
};
