#pragma once

#include "stdafx.h"
#include "Shader.h"
#include "Texture.h"

class ShaderManager {
private:
	static Shader mainShader;
	static Shader spriteShader;
public:
	static void init() {
		loadShaders();
	} 

	static Shader* getMainShader() {
		return &mainShader;
	}

	static Shader* getSpriteShader() {
		return &spriteShader;
	}

private:
	static void loadShaders() {
		mainShader = Shader("vs.glsl", "fs.glsl");

		mainShader.use();
		mainShader.setUniform("textureDiffuse", TEXTURE_DIFFUSE);
		mainShader.setUniform("textureSpecular", TEXTURE_SPECULAR);
		mainShader.setUniform("textureNormals", TEXTURE_NORMALS);

		spriteShader = Shader("vs.sprite.glsl", "fs.sprite.glsl");
		spriteShader.setUniform("image", TEXTURE_DIFFUSE);
	}
};

Shader ShaderManager::mainShader;
Shader ShaderManager::spriteShader;
