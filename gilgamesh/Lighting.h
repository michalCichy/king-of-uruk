#pragma once

#include "stdafx.h"

class Lighting {
public:
	Lighting() {}

	Lighting(float ambient, float diffuse, float specular, vec3 direction) {
		this->ambient = ambient;
		this->diffuse = diffuse;
		this->specular = specular;
		this->direction = direction;
	}

	float ambient;
	float diffuse;
	float specular;

	vec3 direction;
};