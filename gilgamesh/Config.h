#pragma once
#include "stdafx.h"
#include "Constants.h"

enum CameraMode {
	CAMERA_PLAYER,
	CAMERA_GLOBAL
};

class Config {
private:
	static int screenWidth;
	static int screenHeight;

	static CameraMode cameraMode;
	static float cameraFov;
	static float cameraNearPlane;
	static float cameraFarPlane;
	static float cameraMinPitch;
	static float cameraMaxPitch;
	static float cameraSpeed;
	static float cameraBoost;

	static int frameDelay;

	static vec3 sceneLighting; //tymczasowe, normalnie jako atrybut obiektu LightSource
	static vec3 lightDirection;
	static vec3 lightSourcePosition;

	static float mouseSensitivity;

	static string windowTitle;
	static string texturesPath;
	static string modelsPath;

	static vec3 groundDimensions;

	static vec4 skyColor;

	static vec3 playerInitialPosition;
	static float playerSpeed;
	static float playerBoost;

	static int toggleKeyInertia;

public:
	static void setScreenWidth(int value) { screenWidth = value; }
	static void setScreenHeight(int value) { screenHeight = value; }

	static int getScreenWidth() { return screenWidth; }
	static int getScreenHeight() { return screenHeight; }
	
	static void toggleCameraMode() {
		cameraMode == CAMERA_GLOBAL ? cameraMode = CAMERA_PLAYER : cameraMode = CAMERA_GLOBAL;
	}

	static float getCameraMode() { return cameraMode; }
	static float getCameraFov() { return cameraFov;  }
	static float getCameraNearPlane() { return cameraNearPlane; }
	static float getCameraFarPlane() { return cameraFarPlane; }
	static float getCameraMinPitch() { return cameraMinPitch; }
	static float getCameraMaxPitch() { return cameraMaxPitch; }
	static float getCameraSpeed() { return cameraSpeed; }
	static float getCameraBoost() { return cameraBoost; }

	static int getFrameDelay() { return frameDelay; }

	static vec3 getSceneLighting() { return sceneLighting; }
	static vec3 getLightDirection() { return lightDirection; }
	static vec3 getLightSourcePosition() { return lightSourcePosition; }

	static float getMouseSensitivity() { return mouseSensitivity; }

	static string getWindowTitle() { return windowTitle;  }
	static string getTexturesPath() { return texturesPath;  }
	static string getModelsPath() { return modelsPath;  }

	static vec3 getGroundDimensions() { return groundDimensions; }

	static vec4 getSkyColor() { return skyColor; }

	static vec3 getPlayerInitialPosition() { return playerInitialPosition; }
	static float getPlayerSpeed() { return playerSpeed; }
	static float getPlayerBoost() { return playerBoost;  }

	static int getToggleKeyInertia() { return toggleKeyInertia; }
};

int Config::screenWidth = 1920;
int Config::screenHeight = 1080;

CameraMode Config::cameraMode = CAMERA_PLAYER;
float Config::cameraFov = 45;
float Config::cameraNearPlane = 0.1;
float Config::cameraFarPlane = 400;
float Config::cameraMinPitch = -Constants::HALF_PI + 0.1;
float Config::cameraMaxPitch = Constants::HALF_PI - 0.01;
float Config::cameraSpeed = 0.1;
float Config::cameraBoost = 5;

int Config::frameDelay = 15;

vec3 Config::sceneLighting = vec3(0, 0, 1); //tymczasowe, normalnie jako atrybut obiektu LightSource
vec3 Config::lightDirection = vec3(0, 0, -1);
vec3 Config::lightSourcePosition = vec3(0, 0, 10);

float Config::mouseSensitivity = 0.001;

string Config::windowTitle = "Gilgamesh";
string Config::texturesPath = "assets/textures/";
string Config::modelsPath = "assets/models/";

vec3 Config::groundDimensions = vec3(1000, 0.1, 1000);

vec4 Config::skyColor = vec4(0.125, 0.72, 0.85, 1.0);

vec3 Config::playerInitialPosition = vec3(0,0,0);
float Config::playerSpeed = 0.1;
float Config::playerBoost = 5;

int Config::toggleKeyInertia = 100;