//#pragma once
//
//#include "stdafx.h"
//#include "Shader.h"
//#include "Camera.h"
//#include "Mesh.h"
//
//class Cube {
//private:
//	static uint vao;
//public:
//	
//	static void init() {
//		float vertices[] = {
//		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
//		 0.5f, -0.5f, -0.5f,  100.0f, 0.0f,
//		 0.5f,  0.5f, -0.5f,  100.0f, 100.0f,
//		 0.5f,  0.5f, -0.5f,  100.0f, 100.0f,
//		-0.5f,  0.5f, -0.5f,  0.0f, 100.0f,
//		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
//
//		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
//		 0.5f, -0.5f,  0.5f,  100.0f, 0.0f,
//		 0.5f,  0.5f,  0.5f,  100.0f, 100.0f,
//		 0.5f,  0.5f,  0.5f,  100.0f, 100.0f,
//		-0.5f,  0.5f,  0.5f,  0.0f, 100.0f,
//		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
//
//		-0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//		-0.5f,  0.5f, -0.5f,  100.0f, 100.0f,
//		-0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//		-0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
//		-0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//
//		 0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//		 0.5f,  0.5f, -0.5f,  100.0f, 100.0f,
//		 0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//		 0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//		 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
//		 0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//
//		-0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//		 0.5f, -0.5f, -0.5f,  100.0f, 100.0f,
//		 0.5f, -0.5f,  0.5f,  100.0f, 0.0f,
//		 0.5f, -0.5f,  0.5f,  100.0f, 0.0f,
//		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
//		-0.5f, -0.5f, -0.5f,  0.0f, 100.0f,
//
//		-0.5f,  0.5f, -0.5f,  0.0f, 100.0f,
//		 0.5f,  0.5f, -0.5f,  100.0f, 100.0f,
//		 0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//		 0.5f,  0.5f,  0.5f,  100.0f, 0.0f,
//		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
//		-0.5f,  0.5f, -0.5f,  0.0f, 100.0f
//		};
//
//		/*for (int i = 0; i < 30; i+=5) {
//			vertices[i + 3] *= 100.0f;
//			vertices[i + 4] *= 100.0f;
//		}*/
//
//		uint vbo;
//		glGenVertexArrays(1, &vao);
//		glGenBuffers(1, &vbo);
//
//		glBindVertexArray(vao);
//
//		glBindBuffer(GL_ARRAY_BUFFER, vbo);
//		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//
//		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
//		glEnableVertexAttribArray(0);
//
//
//		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
//		glEnableVertexAttribArray(1);
//	}
//
//	static void draw(Shader& shader, Camera& camera, Texture& texture, mat4 transformation) {
//		texture.use();
//		shader.use();
//		shader.setUniform("mvp", camera.getTransformation() * transformation);
//		shader.setUniform("near", Config::getCameraNearPlane());
//		shader.setUniform("far", Config::getCameraFarPlane());
//
//		glBindVertexArray(vao);
//		glDrawArrays(GL_TRIANGLES, 0, 36);
//
//		glBindVertexArray(0);
//
//	}
//};
//
//uint Cube::vao = 0;