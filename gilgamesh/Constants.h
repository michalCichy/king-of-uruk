#pragma once
#include "stdafx.h"

namespace Constants {
	const vec3 WORLD_UP = vec3(0, 1, 0);

	const vec3 X_AXIS = vec3(1, 0, 0);
	const vec3 Y_AXIS = vec3(0, 1, 0);
	const vec3 Z_AXIS = vec3(0, 0, 1);

	const float PI = 3.141592653589793238462643383;
	const float HALF_PI = PI / 2;
	const float DOUBLE_PI = 2 * PI;
};