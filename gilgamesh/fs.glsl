#version 330 core
out vec4 FragColor;

in vec2 uv;
in vec3 normal;
//in vec4 fragmentPosition;

uniform sampler2D textureDiffuse;
uniform sampler2D textureSpecular;
uniform sampler2D textureNormals;

uniform float near;
uniform float far;

uniform vec3 lighting;
uniform vec3 material;
uniform vec3 cameraDirection;
uniform vec3 lightDirection;
uniform vec3 cameraPosition;
uniform vec3 lightSourcePosition;

uniform float ambientLighting;
  
float LinearizeDepth(float depth) 
{
    float z = depth * 2.0 - 1.0;
    return (2.0 * near * far) / (far + near - z * (far - near));	
}

void main()
{
	float depth = 1 - LinearizeDepth(gl_FragCoord.z) / far;
	vec4 textureColor = texture(textureDiffuse, uv);
	//FragColor = texture(textureDiffuse, uv);
	FragColor = vec4(vec3(textureColor * depth * ambientLighting), textureColor.a);
	//float diffuseStrength = dot(-cameraDirection, normal);

	//vec3 viewDirection = normalize(fragmentPosition.xyz - cameraPosition);
	//vec3 reflectionDirection = normalize(reflect((fragmentPosition.xyz - lightSourcePosition), normal)); 
	//float specularStrength = max(dot(-viewDirection, reflectionDirection), 0);
	//FragColor = textureColor * (lighting.x * material.x + lighting.y * material.y * diffuseStrength + material.z * specularStrength);
};