#version 330 core
out vec4 FragColor;

in vec2 uv;

uniform sampler2D image;

uniform float near;
uniform float far;


float LinearizeDepth(float depth) 
{
    float z = depth * 2.0 - 1.0;
    return (2.0 * near * far) / (far + near - z * (far - near));	
}

void main()
{
	vec4 textureColor = texture(image, uv);
	if(textureColor.a == 0) {
		discard;
	}
	float depth = 1 - LinearizeDepth(gl_FragCoord.z) / far;


	FragColor = vec4(vec3(textureColor.rgb) * depth, textureColor.a);
};