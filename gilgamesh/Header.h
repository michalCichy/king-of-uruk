#pragma once

#include "stdafx.h"

class Lighting {
public:
	float ambient;
	float diffuse;
	float specular;

	vec3 direction;
};