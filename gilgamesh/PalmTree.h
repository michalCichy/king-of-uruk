#pragma once

#include "stdafx.h"
#include "Camera.h"
#include "ErrorHandler.h"
#include "ShaderManager.h"

class PalmTree {
private:
	static Mesh quad;
public:
	static void init() {
		objects.push_back(GameObject(Mesh(
			Primitives::Quad::vertices,
			Primitives::Quad::indices,
			ShaderManager::getSpriteShader(),
			TextureManager::getSunTexture(),
			glm::translate(mat4(1), vec3(0, 5, 0)))));
	}

	static void draw(Texture* texture, Camera& camera) {
		texture->use();
		ShaderManager::getSpriteShader()->use();
		ShaderManager::getSpriteShader()->setUniform("near", Config::getCameraNearPlane());
		ShaderManager::getSpriteShader()->setUniform("far", Config::getCameraFarPlane());

		mat4 rotating = glm::rotate(mat4(1), -camera.getYaw() - Constants::HALF_PI, vec3(0, 1, 0)) *
			glm::rotate(mat4(1), camera.getPitch(), vec3(1, 0, 0));
		ShaderManager::getSpriteShader()->setUniform("rotate", rotating);


		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		ErrorHandler::handleErrors();
	}
};

uint Sprite::vao = 0;
int Sprite::indicesCount;