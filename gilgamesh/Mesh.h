#pragma once

#include "stdafx.h"
#include "Vertex.h"
#include "ErrorHandler.h"
#include "Camera.h"
#include "Importer.h"
#include "Texture.h"
#include "Shader.h"

class Mesh {
private:
	Texture* texture = NULL;
	Shader* shader = NULL;
	mat4 initialTransformation = mat4(1);
	mat4 additionalTransformation = mat4(1);

	float pitch = 0;
	float yaw = 0;

	bool rotationsOnly = false;
	bool renderInScreenSpace = false;

	uint vao = 0;
	uint vbo = 0;
	uint ebo = 0;

	int indicesCount = 0;
	

public:
	Mesh() {}

	Mesh(string path, Shader* shader, Texture* texture = NULL, mat4 initialTransformation = mat4(1), int repeatTexture = 1, bool rotationsOnly = false, bool renderInScreenSpace = false) {
		Log::print("Loading model...");
		if (texture != NULL) {
			this->texture = texture;
		}
		this->initialTransformation = initialTransformation;

		vector <RawMesh> rawMeshes;
		Importer::loadMeshes(path, rawMeshes);

		if (rawMeshes.size() > 0) {
			init(rawMeshes[0].vertices, rawMeshes[0].indices, shader, texture, initialTransformation, repeatTexture, rotationsOnly,  renderInScreenSpace);
			cout << "vertices: " << rawMeshes[0].vertices.size() << '\n';
			cout << "indices: " << rawMeshes[0].indices.size() << '\n';
		}
		else {
			Log::print("Failed to load mesh: ");
			Log::print(path);
		}
	}

	Mesh(vector<Vertex>& vertices, vector <uint>& indices, Shader* shader, Texture* texture = NULL, mat4 initialTransformation = mat4(1), int repeatTexture = 1, bool rotationsOnly = false, bool renderInScreenSpace = false) {
		init(vertices, indices, shader, texture, initialTransformation, repeatTexture, rotationsOnly);
	}

	void init(vector<Vertex>& vertices, vector <uint>& indices, Shader* shader, Texture* texture = NULL, mat4 initialTransformation = mat4(1), int repeatTexture = 1, bool rotationsOnly = false, bool renderInScreenSpace = false) {
		cout << "vertices: " << vertices.size() << '\n';
		cout << "indices: " << indices.size() << '\n';

		if (texture != NULL) {
			this->texture = texture;
		}
		else {
			Log::print("No texture passed to Mesh::init. Default will be used.");
		}

		if (shader != NULL) {
			this->shader = shader;
		}
		else {
			Log::print("Invalid shader passed to Mesh::init");
		}

		this->initialTransformation = initialTransformation;

		this->rotationsOnly = rotationsOnly;
		this->renderInScreenSpace = renderInScreenSpace;

		indicesCount = indices.size();

		if (repeatTexture != 1) {
			for (Vertex& vertex : vertices) {
				vertex.uv *= repeatTexture;
			}
		}

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);

		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(
			GL_ARRAY_BUFFER,
			vertices.size() * sizeof(Vertex),
			&vertices[0],
			GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(
			GL_ELEMENT_ARRAY_BUFFER,
			indices.size() * sizeof(uint),
			&indices[0],
			GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(Vertex), (void*)(sizeof(vec3)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, sizeof(Vertex), (void*)(sizeof(vec3) + sizeof(vec2)));
		glEnableVertexAttribArray(2);

		glBindVertexArray(0);

		ErrorHandler::handleErrors();
	}

	void rotatePitch(float pitch) {
		this->pitch += pitch;

		if (pitch > Constants::DOUBLE_PI) {
			pitch -= Constants::DOUBLE_PI;
		}
		else if (pitch < -Constants::DOUBLE_PI) {
			pitch += Constants::DOUBLE_PI;
		}
	}

	void rotateYaw(float yaw) {
		this->yaw += yaw;

		if (yaw > Constants::DOUBLE_PI) {
			yaw -= Constants::DOUBLE_PI;
		}
		else if (yaw < -Constants::DOUBLE_PI) {
			yaw += Constants::DOUBLE_PI;
		}
	}

	void setPitch(float pitch) {
		this->pitch = pitch;
	}

	void setYaw(float yaw) {
		this->yaw = yaw;
	}

	void draw(Camera& camera, Lighting& lighting) {
		if (texture != NULL) {
			texture->use();
		}
		if (shader != NULL) {
			shader->use();
		}

		mat4 cameraTransformation;
		if (rotationsOnly == true) {
			cameraTransformation = camera.getRotations();
		}
		else if (renderInScreenSpace == true) {
			cameraTransformation = camera.getPerspective();
		}
		else {
			cameraTransformation = camera.getTransformation();
		}

		shader->setUniform("mvp", cameraTransformation * glm::rotate(mat4(1), pitch, Constants::X_AXIS) * glm::rotate(mat4(1), yaw, Constants::Y_AXIS)  * initialTransformation);
		shader->setUniform("near", Config::getCameraNearPlane());
		shader->setUniform("far", Config::getCameraFarPlane());

		mat4 lookTowardsCamera = glm::rotate(mat4(1), -camera.getYaw() - Constants::HALF_PI, vec3(0, 1, 0)) *
			glm::rotate(mat4(1), camera.getPitch(), vec3(1, 0, 0));	
		shader->setUniform("lookTowardsCamera", lookTowardsCamera); //u�ywane tylko dla sprit�w
		
		shader->setLighting(lighting);

		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
	}
};