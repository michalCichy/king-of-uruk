#pragma once

#include<iostream>
#include<fstream>
#include<cstdio>
#include<vector>
#include<map>
#include<string>

#include<windows.h>

#include<glad/glad.h>
#include<GLFW/glfw3.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/constants.hpp>
#include<stb_image.h>

using std::cout;
using std::vector;
using std::map;
using std::string;
using std::to_string;
using std::ifstream;
using std::getline;

using glm::mat3;
using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;

#define uint unsigned int

#include "Log.h"